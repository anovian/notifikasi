package com.artivisi.training.microservice201901.notifikasi.service;

import com.artivisi.training.microservice201901.notifikasi.dto.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KafkaHandlerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaHandlerService.class);

    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.notification}")
    public void terimaNotifikasi(String msg) {
        LOGGER.debug("Notifikasi dari kafka : {}", msg);

        try {
            Notification notif = objectMapper.readValue(msg, Notification.class);
            LOGGER.debug("Object notif : {}", notif);

            LOGGER.info("Kirim email dari {}, ke {}, dengan subject {}, isi email : {}",
                    notif.getFrom(), notif.getTo(), notif.getSubject(), notif.getMessage());

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
