package com.artivisi.training.microservice201901.notifikasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotifikasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotifikasiApplication.class, args);
	}

}

