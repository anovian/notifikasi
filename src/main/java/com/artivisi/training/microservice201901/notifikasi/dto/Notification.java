package com.artivisi.training.microservice201901.notifikasi.dto;

import lombok.Data;

@Data
public class Notification {
    private String type;
    private String from;
    private String to;
    private String subject;
    private String message;
}
